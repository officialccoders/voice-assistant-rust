# SADE Voice Assistant Rust

SADE is a rust voice assistant which is was made to be easy to set up, customize and learn from.

## Installation
### Download LLM Model

[Install Ollama](https://ollama.ai/download)\
[Pull Orca-Mini](https://ollama.ai/library/orca-mini:3b)

On Linux:
```
curl https://ollama.ai/install.sh | sh
ollama pull orca-mini:3b
```

### Run SADE Voice Assistant

```
cd path/to/voice-assistant-rust/
cargo run
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)